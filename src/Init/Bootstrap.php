<?php
/**
 * @file Bootstrap.php
 * @version 0.7
 * - Class responsavel por organizar e gerenciar as requições passadas por URLs amigaveis
 * setando os Controllers, Actions e Parâmetros
 */

namespace HTR\Init;

class Bootstrap
{

    /**
     * Recebe o valor da url
     *
     * @var array
     */
    private $url;

    /**
     * Recebe o nome do Controller
     *
     * @var string
     */
    private $controller;

    /**
     * Recebe o nome da Action
     *
     * @var string
     */
    private $action;

    /**
     * Recebe os parâmetros enviados pela URL
     *
     * @var array
     */
    private $params = [];

    public function __construct()
    {
        $this->setUrl(filter_input(INPUT_GET, 'url'))
            ->setController()
            ->setAction()
            ->setParam();
    }

    /**
     * Seta a URL
     *
     * @return \HTR\Init\Bootstrap
     */
    private function setUrl($url)
    {
        // Controller e Action padrão
        $this->url = explode('/', isset($url) ? $url : 'Index/index' );
        return $this;
    }

    /**
     * Seta o nome do Controller
     *
     * @return \HTR\Init\Bootstrap
     */
    private function setController()
    {
        $url = $this->url;
        $this->controller = ucfirst($url[0]) . 'Controller';
        return $this;
    }

    /**
     * Seta o nome da Action
     *
     * @return \HTR\Init\Bootstrap
     */
    private function setAction()
    {
        $url = $this->url;
        $this->action = !empty($url[1]) ? strtolower($url[1]) . 'Action' : 'indexAction';
        return $this;
    }

    /**
     * Seta os parâmetros enviados pela URL
     *
     * @return \HTR\Init\Bootstrap
     */
    private function setParam()
    {
        // recupera o valor dos parâmetros
        $url = $this->url;

        // elimina os valores do controller e action
        unset($url[0], $url[1]);

        //verifica se o ultimo elemento está vazio
        // então o elimina do array
        if (empty(end($url))) {
            array_pop($url);
        }

        // divide o array por combinações de [chave = valor]
        $arrParams = array_chunk($url, 2);

        // percorre o novo array para extração das tuplas
        foreach ($arrParams as $tuple) {
            // caso não exista o valor correspondente para chave
            // então pula uma iteração no array
            if (!isset($tuple[1])) {
                continue;
            }

            // adiciona para o array de parâmetros os valores repassados
            $this->params[$tuple[0]] = $tuple[1];
        }

        return $this;
    }

    /**
     * Retorna os parâmetros setados pela URL
     *
     * @param mixed|bool $key Nome/Chave do Parâmetro
     * @return array Array de parâmetros
     */
    public function getParam($key = null)
    {
        /**
         * Se o Parâmetro $key for idêntico a true, então devolve o array de URL completo
         */
        if ($key === true) {
            return $this->url;
        }

        // se o parâmetro for diferente de NULL
        // o script executará a função array_key_exists
        if ($key) {
            /**
             * verifica se a chave requisitada existe no Array de parâmetros
             * se existir, retorna o parâmetro com a chave indicada, caso
             * contrário, retornará NULL
             */
            return array_key_exists($key, $this->params) ? $this->params[$key] : null;
        }

        // retorna o Array de parâmteros completo
        return $this->params;
    }

    /**
     * Inicia a aplicação
     */
    protected function run()
    {
        if (file_exists(DRINST . 'App/Controllers/' . $this->controller . '.php')) {

            $class = "App\\Controllers\\" . $this->controller;

            // instacia o Controller e Injeta o Objeto de \HTR\Init\Bootstrap
            $controller = new $class($this);

            $controllerInterface = \HTR\Interfaces\ControllerInterface::class;

            if (!($controller instanceof $controllerInterface)) {
                throw new \Exception("Os Controllers devem implentar a Interface: " . $controllerInterface);
            }

            // verifica se o método é existente
            if (!method_exists($class, $this->action)) {
                // caso o método (action) não exista, retorna um Erro 404
                new \HTR\Helpers\ErrorPag\ErrorPag('error_404');
            }

            // retorna  a Action
            $action = $this->action;
            // executa um método intermediário antes de chamar a action
            $controller->middleware();
            // executa a Action
            $controller->$action();
        } else {
            // caso o Controller não exista, retorna um Erro 404
            new \HTR\Helpers\ErrorPag\ErrorPag('error_404');
        }
    }
}
