<?php

namespace HTR\Database;

use HTR\Database\Sgbd\SgbdAbstract;

/**
 * @file SgdbConnectionFactory.php
 * @version 0.1
 * - Interface de conexão usada no sistema
 */
class SgbdConnectionFactory
{
    private $config;
    private $instance;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->rules()
            ->run();
    }

    private function rules()
    {
        if (!isset($this->config['sgbd'])) {
            throw new \Exception("Não foi possível detectar o SGBD. "
                . "Verifique o arquivo de configuração de conexão do HTR.");
        }

        $className = "\HTR\Database\Sgbd\\" . ucfirst(strtolower($this->config['sgbd']));

        if (!class_exists($className)) {
            throw new \Exception("Não foi possível Encontrar a Class {$this->config['sgbd']}. "
                . "Verifique o arquivo de configuração de conexão do HTR.");
        }

        $this->instance = new $className($this->config);

        if (!($this->instance instanceof SgbdAbstract)) {
            throw new \Exception("A Class {$this->config['sgbd']} não é filha de SgbdAbstract. "
                . "Verifique o arquivo de configuração de conexão do HTR.");
        }

        return $this;
    }

    private function run()
    {
        $this->instance->run();
    }

    final public function getConnection()
    {
        return $this->instance->getConnection();
    }
}
