<?php

namespace HTR\Database\Sgbd;

use HTR\Database\Sgbd\SgbdAbstract;

/**
 * @file Sqlite.php
 * @version 0.1
 * - Class reposnsável pela conexão com o banco de dados Sqlite
 */
class Sqlite extends SgbdAbstract
{
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function run()
    {
        $this->rules();

        $this->connect();
    }

    protected function rules()
    {
        if (is_array($this->config)) {

            if (empty($this->config['server'])) {
                throw new \Exception('Você não informou o nome do Arquivo Sqlite');
            }

            return;
        }

        throw new \Exception('Configuração de conexão com o Banco de Dados Inválida.');
    }

    protected function connect()
    {

        try {

            $this->connection = new \PDO('sqlite:' . DATADR . base64_decode($this->config['server']));

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {
            throw new \Exception('Erro ao conectar. Código: '.$e->getCode(). '! Mensagem: '.$e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->connection;
    }
}
