<?php

namespace HTR\Database\Sgbd;

use HTR\Database\Sgbd\SgbdAbstract;

/**
 * @file Mysql.php
 * @version 0.1
 * - Class reposnsável pela conexão com o banco de dados MySQL
 */
class Mysql extends SgbdAbstract
{
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function run()
    {
        $this->rules();

        $this->connect();
    }

    protected function rules()
    {
        if (is_array($this->config)) {

            if (empty($this->config['server'])) {
                throw new \Exception('Você não informou o servidor!');
            }

            if (empty($this->config['dbname'])) {
                throw new \Exception('Você não informou o banco de dados!');
            }

            if (empty($this->config['username'])) {
                throw new \Exception('Você não informou o usuário!');
            }

            if (!isset($this->config['password'])) {
                throw new \Exception('Você não informou a senha!');
            }

            if (!isset($this->config['options']) or !is_array($this->config['options'])) {
                throw new \Exception('Você não informou as opções ou não é '
                    . 'um array, você precisa informar isso mesmo que vazio!');
            }

            return;
        }

        throw new \Exception('Configuração de conexão com o Banco de Dados Inválida.');
    }

    protected function connect()
    {

        try {
            $this->connection = new \PDO(
                'mysql:host=' . base64_decode($this->config['server'])
                . ';dbname=' .base64_decode($this->config['dbname']),
                base64_decode($this->config['username']),
                base64_decode($this->config['password']),
                $this->config['options']
            );

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {
            throw new \Exception('Erro ao conectar. Código: '.$e->getCode(). '! Mensagem: '.$e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->connection;
    }
}
