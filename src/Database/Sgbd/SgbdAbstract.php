<?php

namespace HTR\Database\Sgbd;

/**
 * @file SgbdAbstract.php
 * @version 0.1
 * - Regras e métodos comuns a todos os SGDBs reconhecidos pelo sistema
 */
abstract class SgbdAbstract
{
    protected $config;

    protected $connection;

    abstract public function run();

    abstract protected function rules();

    abstract protected function connect();

    abstract public function getConnection();
}
