<?php

/**
 * @file Database.php
 * @version 0.6
 * - Class que Executa, Seleciona a Instrução e Gerencia a Conexão com o Banco de Dados
 */
namespace HTR\Database;

use HTR\Database\Instruction;
use HTR\Database\SgbdConnectionFactory;

class Database
{

    /**
     * Configurações de conexão
     *
     * @var array
     */
    private $config;
    /**
     * Recebe uma instância do PDO
     *
     * @var \PDO
     */
    private $pdo;
    /**
     * Recebe a instância da instrução requisitada
     * que podem ser: Insert, Update, Select, Delete
     */
    private $instruction;

    /**
     * Construtor da Class
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     *
     * @param \PDO $pdo Recebe um objeto PDO de uma conexão realizar anteriormente
     * @return type \PDO Objeto PDO
     * @throws \Exception
     */
    public function connect(\PDO $pdo = null)
    {
        if ($pdo != null) {
            // reaproveita a conexão aberta anteriormente
            $this->pdo = $pdo;
            return $this->pdo;
        }

        $sgbdFactory = new SgbdConnectionFactory($this->config);
        $this->pdo = $sgbdFactory->getConnection();

        return $this->pdo;
    }

    /**
     * Seta as instruções necessárias para a execução do CRUD
     *
     * @param Instruction $instruction Instância da instrução a ser execuatada
     * @param string $entity Nome da entidade usada na operação
     * @return Select|Update|Inserte|Delete
     */
    public function instruction(Instruction $instruction)
    {
        return $this->instruction = $instruction;
    }

    /**
     * Executa a query staring
     * @return \PDOStatement
     */
    public function execute()
    {
        $stmt = $this->pdo->prepare($this->instruction->returnSql());
        $binds = $this->instruction->returnBind();
        $stmt->execute(array_values($binds));

        return $stmt;
    }
}
