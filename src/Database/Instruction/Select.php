<?php

/**
 * @file Select.php
 * @version 0.3
 * - Class que gerencia a seleção de registros do Banco de Dados
 */
namespace HTR\Database\Instruction;

use HTR\Database\Instruction;

final class Select extends Instruction
{

    /**
     * Campos usados na Seleção de registros do Banco de Dados
     *
     * @var array
     */
    private $fields;

    private $join = null;

    public function __construct($entity)
    {
        $this->setEntity($entity);
    }

    /**
     * Executa um JOIN com outras tabelas. Por default é passado INNER JOIN
     * Mas tudo pode ser alterado passando um callback no terceiro parâmetro
     * @param string $entityJoin Entidade a qual deseja juntar-se
     * @param type $relationship Campos responsáveis pelo relacionamento
     * @param callable $func callback usado como alternativa para resultados expecíficos
     * @return \HTR\Database\Instruction\Select
     * @throws \Exception
     */
    public function join($entityJoin, $relationship, callable $func = null)
    {
        if (empty($entityJoin) || empty($relationship)) {
            throw new \Exception('Para fazer um JOIN com outra entidade é '
                    . 'necessário fornecer o nome da entidade e a relação.');
        }

        if (!empty($func) && !is_callable($func)) {
            throw new \Exception('O Callback passado para JOIN não é válido');
        }

        if ($func) {
            $this->join[] = $func($entityJoin, $relationship);
            return $this;
        }

        $this->join[] = "INNER JOIN " .  $entityJoin . " ON " . $entityJoin . '.' . $relationship;
        return $this;
    }

    /**
     * Configura os campos para busca no Banco de Dados
     *
     * @param array $fields
     * @return \HTR\Database\Instruction\Select
     */
    public function setFields(array $fields)
    {
        $this->fields = implode(', ', $fields);
        return $this;
    }

    /**
     * Retorna a instrução SQL
     *
     * @return string
     * @throws \Exception
     */
    public function returnSql()
    {
        $this->fields = empty($this->fields) ? '*' : $this->fields;

        if (empty($this->entity)) {
            throw new \Exception('Você não declarou a entidade!');
        }

        if ($this->join) {
            $this->join = implode(' ', $this->join);
        }

        $sql = "SELECT {$this->fields} FROM {$this->entity} {$this->join}";

        if (!empty($this->filters)) {
            $sql .= ' ' . $this->filters->returnSql();
        }

        return $sql . ';';
    }

    /**
     * @param array $values
     * @throws \Exception
     */
    public function setValues(array $values = [])
    {
        throw new \Exception('Você não pode chamar o método setaValores em um Select!');
    }
}