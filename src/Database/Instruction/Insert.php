<?php

/**
 * @file Insert.php
 * @version 0.4
 * - Class que gerencia a inserção de registros do Banco de Dados
 */
namespace HTR\Database\Instruction;

use HTR\Database\Instruction;

final class Insert extends Instruction
{
    public function __construct($entity)
    {
        $this->setEntity($entity);
    }

    /**
     * Valores usados na inserção de dados no Banco de Dados
     *
     * @var string
     */
    private $values;

    /**
     * Retorna a instrução SQL
     *
     * @return string
     * @throws \Exception
     */
    public function returnSql()
    {
        if (empty($this->entity)) {
            throw new \Exception('Você não declarou a entidade!');
        }

        $sql = "INSERT INTO {$this->entity} {$this->values};";
        return $sql;
    }

    /**
     * Configura os valores da instrução SQL
     *
     * @param array $values
     * @return \HTR\Database\Instruction\Insert
     */
    public function setValues(array $values = [])
    {
        parent::setValues($values);
        $keys = array_keys($values);
        $column = implode(', ', $keys);
        $values = [];

        foreach ($keys as $val) {
            $values[] = '?';
        }

        $values = implode(', ', $values);

        $this->values = '(' . $column . ') VALUES (' . $values . ')';

        return $this;
    }
}