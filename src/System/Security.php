<?php

/**
 * @file Secutiry.php
 * @version 0.1
 * Lib que reune os métodos de proteção contra CSRF
 */
namespace HTR\System;

use HTR\Helpers\Session\Session;
use HTR\Helpers\Mensagem\Mensagem as msg;

class Security
{
    private $hashValue;
    private $hashField;

    private function makeHashToken()
    {
        $session = new Session();
        $session->startSession();

        $srtSal = STRSAL . time();

        $this->hashField = md5($srtSal . uniqid());
        $this->hashValue = sha1($srtSal . uniqid());

        //$_SESSION['token'] = $session->getToken();
        $_SESSION['_tokenId'] = $this->hashField;
        $_SESSION['_tokenValue'] = $this->hashValue;

        return $this;
    }

    final public function fieldToken()
    {
        $this->makeHashToken();
        return "<input type=\"hidden\" name=\"{$this->hashField}\" value=\"{$this->hashValue}\">";
    }

    public function checkToken($makeHashToken = false)
    {
        $session = new Session();
        $session->startSession();

        if (filter_input(INPUT_POST, $_SESSION['_tokenId']) !=  $_SESSION['_tokenValue']) {
            msg::showMsg('Houve um erro ao submeter os dados do formulário', 'warning');
        }

        if ($makeHashToken) {
            $this->makeHashToken();
        }
    }
}
