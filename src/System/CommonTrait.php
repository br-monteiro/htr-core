<?php

/**
 * @file Common.php
 * @version 0.1
 * Lib que reune os métodos comuns do sistema
 */
namespace HTR\System;

use HTR\Database\Instruction\Select;

trait CommonTrait
{
    public function withJson()
    {
        return json_encode($this->result);
    }

    /**
     * Método usado como curinga para realiza uma busca no Banco de Dados
     * por um campo específico; Como também setar e retornar valores de atributos
     * 
     * @param string $method Nome do Método requisitado
     * @param array|null $properties Propriedades do Método
     * @return \HTR\System\ModelCRUD
     * @throws \Exception
     */
    public function __call($method, $properties = null)
    {
        if (substr($method, 0, 6) == 'findBy') {

            $campo = strtolower(substr($method, 6, strlen($method)));
            $this->db->instruction(new Select($this->entidade))
                ->setFilters()
                ->where($campo, '=', isset($properties[0]) ? $properties[0] : null);

            $this->result = $this->db->execute()->fetch(\PDO::FETCH_ASSOC);
            return $this->result;
            
        }
        
        if (substr($method, 0, 9) == 'findAllBy') {

            $campo = strtolower(substr($method, 9, strlen($method)));
            $this->db->instruction(new Select($this->entidade))
                ->setFilters()
                ->where($campo, '=', isset($properties[0]) ? $properties[0] : null);

            $this->result = $this->db->execute()->fetchAll(\PDO::FETCH_ASSOC);
            return $this->result;

        }
        
        if (substr($method, 0, 8) == 'searchBy') {

            $campo = strtolower(substr($method, 8, strlen($method)));
            $this->db->instruction(new Select($this->entidade))
                ->setFilters()
                ->where($campo, 'LIKE', isset($properties[0]) ? '%' . $properties[0] . '%' : null);

            $this->result = $this->db->execute()->fetchAll(\PDO::FETCH_ASSOC);
            return $this->result;
            
        }
        
        if (substr($method, 0, 3) == 'set') {
            
            $attributeName = lcfirst(substr($method, 3, strlen($method)));
            $this->$attributeName = isset($properties[0]) ? $properties[0] : null;
            return $this;
            
        }
        
        if (substr($method, 0, 3) == 'get') {
            
            $attributeName = lcfirst(substr($method, 3, strlen($method)));
            return $this->$attributeName;
            
        }
        
        throw new \Exception('Método não encontrado');
        
    }
}
