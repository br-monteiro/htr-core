<?php

/**
 * @file ModelCRUD.php
 * @version 0.5
 * - Class que gerencia a abstração do Banco de Dados - CRUD
 */
namespace HTR\System;

use HTR\System\ModelAbstract;
use HTR\Database\Instruction\Select;
use HTR\Database\Instruction\Insert;
use HTR\Database\Instruction\Update;
use HTR\Database\Instruction\Delete;

class ModelCRUD extends ModelAbstract
{

    use CommonTrait;

    private $result;

    /**
     * Retorna todos os dados
     *
     * @param string $orderBy Modo a ser ordenado os resultados obtidos pela consulta
     * @return array Retorna os resultados enontrados na consulta SQL
     */
    public function findAll($orderBy = null)
    {
        $select = $this->db->instruction(new Select($this->entidade));

        if ($orderBy) {
            $select->setFilters()
                ->orderBy($orderBy);
        }

        $this->result = $this->db->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $this->result;

    }

    /**
     * Insere novos dados no Banco de Dados
     *
     * @param array $dados Dados a serem tratados na execução
     * @return \PDOStatement
     */
    public function insert(array $dados)
    {
        $this->db->instruction(new Insert($this->entidade))
            ->setValues($dados);

        return $this->db->execute();
    }

    /**
     * Altera dados do Banco de Dados
     *
     * @param array $dados Dados a serem tratados na execução
     * @param mixed $id Id da(s) linha(s) a ser(em) alterada(s)
     * @return \PDOStatement
     */
    public function update(array $dados, $id)
    {
        $this->db->instruction(new Update($this->entidade))
            ->setValues($dados)
            ->setFilters()
            ->where('id', '=', $id);

        return $this->db->execute();
    }

    /**
     * Remove dados do Banco de Dados
     *
     * @param mixed $id Id da(s) linha(s) a ser(em) alterada(s)
     * @return \PDOStatement
     */
    public function delete($id)
    {
        $this->db->instruction(new Delete($this->entidade))
            ->setFilters()
            ->where('id', '=', $id);

        return $this->db->execute();
    }

}
