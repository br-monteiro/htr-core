<?php
/**
 * @file ControllerAbstract.php
 * @version 0.6
 * - Class responsavel por gerenciar os Controllers da Aplicação
 */

namespace HTR\System;

use HTR\System\Security;
use HTR\System\Views;

class ControllerAbstract
{

    protected $view;
    protected $token;
    private $viewEngine;
    private $bootstrapObject;

    public function __construct(\HTR\Init\Bootstrap $bootstrapDI = null)
    {
        $this->bootstrapObject = $bootstrapDI;
        $this->token = new Security();
        $this->viewEngine = new Views();
    }

    final public function views()
    {
        return $this->viewEngine;
    }

    final public function render($view, array $value = [])
    {
        $value = array_merge($value, $this->view);

        $value['token'] = $this->token->fieldToken();

        $this->viewEngine->render($view, $value);
    }

    /**
     * Retorna os parâmetros setados na URL
     * Se o valor de $key for idêntico a true, então retonar o array de URL completo
     * 
     * @param mixed $key Nome do Parâmetro ou Chave do array
     * @return array|string Retorna o parâmetro requisitado
     */
    protected function getParam($key = null)
    {
        return $this->bootstrapObject->getParam($key);
    }

    /**
     * Método intermediário executado antes das actions
     */
    public function middleware()
    {
        // todo
    }
}
