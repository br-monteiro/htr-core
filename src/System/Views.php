<?php
/**
 * @file Views.php
 * @version 0.1
 * Renderiza as Views do sistema
 */
namespace HTR\System;

use Philo\Blade\Blade;

class Views extends Blade
{
    public function __construct()
    {
        parent::__construct(DIRVIEW, DIRCACHE);
    }
    
    final public function render($view, array $value = [])
    {
        echo $this->view()->make($view, $value)->render();
    }
}
