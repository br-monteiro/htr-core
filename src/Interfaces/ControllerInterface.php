<?php

/**
 * @file ControllerInterface.php
 * @version 0.2
 * - Interface padrão para Controllers
 */
namespace HTR\Interfaces;

interface ControllerInterface
{
    /**
     * Método padrão que todo Controller deve ter
     */
    public function indexAction();
    
    public function __construct($bootstrapDI);
}
